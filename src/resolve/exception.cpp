#include "resolve/exception.hpp"

#include <sstream>

namespace resolve
{

    duplicate_interface_error::duplicate_interface_error(std::type_index type)
        : m_type{type}
    {
    }

    auto duplicate_interface_error::type_index() const noexcept -> std::type_index
    {
        return m_type;
    }

    auto duplicate_interface_error::what() const noexcept -> const char *
    {
        if (m_what.empty()) {
            m_what = (std::ostringstream{} << "duplicate interface: " << m_type.name()).str();
        }
        return m_what.c_str();
    }

    unknown_interface_error::unknown_interface_error(std::type_index type)
        : m_type{type}
    {
    }

    auto unknown_interface_error::type_index() const noexcept -> std::type_index
    {
        return m_type;
    }

    auto unknown_interface_error::what() const noexcept -> const char *
    {
        if (m_what.empty()) {
            m_what = (std::ostringstream{} << "unknown interface: " << m_type.name()).str();
        }
        return m_what.c_str();
    }
}
