#include "resolve/exception.hpp"

#include <string_view>
#include <typeindex>
#include <typeinfo>

#include <gtest/gtest.h>

namespace resolve
{
    TEST(exception, duplicate_interface_error)
    {
        auto t = std::type_index{typeid(void)};
        auto e = duplicate_interface_error{t};

        EXPECT_EQ(e.type_index(), t);

        auto s = std::string_view{e.what()};
        EXPECT_FALSE(s.empty());
    }

    TEST(exception, unknown_interface_error)
    {
        auto t = std::type_index{typeid(void)};
        auto e = unknown_interface_error{t};

        EXPECT_EQ(e.type_index(), t);

        auto s = std::string_view{e.what()};
        EXPECT_FALSE(s.empty());
    }
}
