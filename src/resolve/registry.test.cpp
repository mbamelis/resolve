#include "resolve/registry.hpp"

#include "resolve/component.hpp"
#include "resolve/exception.hpp"
#include "resolve/registrar.hpp"

#include <gtest/gtest.h>

namespace resolve
{
    namespace
    {
        struct interface
        {
        };

        struct dependency
            : interface
            , simple_component
        {
        };

        struct dependent
        {
            interface *dependency;
            auto connect(const registry &r)
            {
                r.get(dependency);
            }
        };
    }

    TEST(registry, connect_simple)
    {
        auto r = registrar{};

        // Order of insertion does not matter
        r.insert(dependent{});
        r.insert<interface>(dependency{});

        ASSERT_NO_THROW(r.resolve());
    }

    TEST(registry, connect_merged)
    {
        auto r = registrar{};

        r.insert<interface>(dependency{});
        r.insert(r.resolve());

        auto &c = r.insert<interface>(dependency{});
        auto &d = r.insert(dependent{});

        ASSERT_NO_THROW(r.resolve());
        ASSERT_EQ(d.dependency, &c);
    }
}
