#include "resolve/registrar.hpp"

#include "resolve/component.hpp"
#include "resolve/exception.hpp"
#include "resolve/registry.hpp"

#include <gtest/gtest.h>

namespace resolve
{
    namespace
    {
        struct interface
        {
        };

        struct simple_component1
            : interface
            , simple_component
        {
        };

        struct simple_component2
            : interface
            , simple_component
        {
        };

        struct counting_component
            : simple_component1
            , simple_component2
        {
            int connect_count{};

            auto connect(const registry &r)
            {
                ++connect_count;
            }
        };
    }

    TEST(registrar, insert_val)
    {
        auto r = registrar{};

        ASSERT_FALSE(r.contains<simple_component1>());
        ASSERT_FALSE(r.contains<simple_component2>());
        ASSERT_FALSE(r.contains<interface>());

        r.insert(simple_component1{});
        EXPECT_TRUE(r.contains<simple_component1>());
        EXPECT_FALSE(r.contains<simple_component2>());
        EXPECT_FALSE(r.contains<interface>());

        r.insert(simple_component2{});
        EXPECT_TRUE(r.contains<simple_component1>());
        EXPECT_TRUE(r.contains<simple_component2>());
        EXPECT_FALSE(r.contains<interface>());

        r.insert<interface>(simple_component1{});
        EXPECT_TRUE(r.contains<simple_component1>());
        EXPECT_TRUE(r.contains<simple_component2>());
        EXPECT_TRUE(r.contains<interface>());

        EXPECT_THROW(r.insert(simple_component1{}), duplicate_interface_error);
        EXPECT_THROW(r.insert(simple_component2{}), duplicate_interface_error);
        EXPECT_THROW(r.insert<interface>(simple_component1{}), duplicate_interface_error);
    }

    TEST(registrar, insert_ref)
    {
        auto r = registrar{};
        auto c1 = simple_component1{};
        auto c2 = simple_component2{};

        ASSERT_FALSE(r.contains<simple_component1>());
        ASSERT_FALSE(r.contains<simple_component2>());
        ASSERT_FALSE(r.contains<interface>());

        r.insert(c1);
        EXPECT_TRUE(r.contains<simple_component1>());
        EXPECT_FALSE(r.contains<simple_component2>());
        EXPECT_FALSE(r.contains<interface>());

        r.insert(c2);
        EXPECT_TRUE(r.contains<simple_component1>());
        EXPECT_TRUE(r.contains<simple_component2>());
        EXPECT_FALSE(r.contains<interface>());

        r.insert<interface>(c1);
        EXPECT_TRUE(r.contains<simple_component1>());
        EXPECT_TRUE(r.contains<simple_component2>());
        EXPECT_TRUE(r.contains<interface>());

        EXPECT_THROW(r.insert(c1), duplicate_interface_error);
        EXPECT_THROW(r.insert(c2), duplicate_interface_error);
    }

    TEST(registrar, emplace)
    {
        auto r = registrar{};

        ASSERT_FALSE(r.contains<simple_component1>());
        ASSERT_FALSE(r.contains<simple_component2>());
        ASSERT_FALSE(r.contains<interface>());

        r.emplace<simple_component1>();
        EXPECT_TRUE(r.contains<simple_component1>());
        EXPECT_FALSE(r.contains<simple_component2>());
        EXPECT_FALSE(r.contains<interface>());

        r.emplace<simple_component2>();
        EXPECT_TRUE(r.contains<simple_component1>());
        EXPECT_TRUE(r.contains<simple_component2>());
        EXPECT_FALSE(r.contains<interface>());

        r.emplace<interface, simple_component1>();
        EXPECT_TRUE(r.contains<simple_component1>());
        EXPECT_TRUE(r.contains<simple_component2>());
        EXPECT_TRUE(r.contains<interface>());

        EXPECT_THROW(r.emplace<simple_component1>(), duplicate_interface_error);
        EXPECT_THROW(r.emplace<simple_component2>(), duplicate_interface_error);
        EXPECT_THROW((r.emplace<interface, simple_component1>()), duplicate_interface_error);
    }

    TEST(registrar, make_and_insert)
    {
        auto r = registrar{};

        ASSERT_FALSE(r.contains<simple_component1>());
        ASSERT_FALSE(r.contains<simple_component2>());
        ASSERT_FALSE(r.contains<interface>());

        auto c1 = r.make_and_insert<simple_component1>();
        EXPECT_TRUE(r.contains<simple_component1>());
        EXPECT_FALSE(r.contains<simple_component2>());
        EXPECT_FALSE(r.contains<interface>());

        auto c2 = r.make_and_insert<simple_component2>();
        EXPECT_TRUE(r.contains<simple_component1>());
        EXPECT_TRUE(r.contains<simple_component2>());
        EXPECT_FALSE(r.contains<interface>());

        auto c3 = r.make_and_insert<interface, simple_component1>();
        EXPECT_TRUE(r.contains<simple_component1>());
        EXPECT_TRUE(r.contains<simple_component2>());
        EXPECT_TRUE(r.contains<interface>());

        EXPECT_THROW(r.make_and_insert<simple_component1>(), duplicate_interface_error);
        EXPECT_THROW(r.make_and_insert<simple_component2>(), duplicate_interface_error);
        EXPECT_THROW((r.make_and_insert<interface, simple_component1>()), duplicate_interface_error);
    }

    TEST(registrar, resolve_simple)
    {
        auto r = registrar{};
        auto c = counting_component{};

        r.insert(c);
        EXPECT_TRUE(r.contains<counting_component>());
        EXPECT_FALSE(r.contains<simple_component1>());
        EXPECT_FALSE(r.contains<simple_component2>());

        r.insert<simple_component1>(c);
        r.insert<simple_component2>(c);
        EXPECT_TRUE(r.contains<simple_component1>());
        EXPECT_TRUE(r.contains<simple_component2>());

        r.resolve();
        EXPECT_EQ(c.connect_count, 1);
    }

    TEST(registrar, resolve_merged)
    {
        auto r = registrar{};
        auto c = counting_component{};

        r.insert(c);
        EXPECT_TRUE(r.contains<counting_component>());
        EXPECT_FALSE(r.contains<simple_component1>());
        EXPECT_FALSE(r.contains<simple_component2>());

        r.insert(r.resolve());
        EXPECT_EQ(c.connect_count, 1);
        EXPECT_TRUE(r.contains<counting_component>());
        EXPECT_FALSE(r.contains<simple_component1>());
        EXPECT_FALSE(r.contains<simple_component2>());

        r.insert<simple_component1>(c);
        r.insert<simple_component2>(c);
        EXPECT_TRUE(r.contains<simple_component1>());
        EXPECT_TRUE(r.contains<simple_component2>());

        r.insert(r.resolve());
        EXPECT_EQ(c.connect_count, 1);

        r.resolve();
        EXPECT_EQ(c.connect_count, 1);
    }
}
