#include "resolve/registrar.hpp"

#include "resolve/registry.hpp"

#include <unordered_set>
#include <utility>

namespace resolve
{
    auto registrar::resolve() -> registry
    {
        if (m_external.empty()) {
            return std::exchange(*this, registrar{}).resolve_simple();
        }
        else {
            return std::exchange(*this, registrar{}).resolve_merged();
        }
    }

    auto registrar::insert(registry &&r) -> void
    {
        m_external.merge(std::move(r.m_components));
    }

    /**
     * @brief Resolves a registrar with zero external components.
     */
    auto registrar::resolve_simple() -> registry
    {
        auto unresolved = std::unordered_map<void *, component_base *>{};
        unresolved.reserve(m_internal.size());

        for (const auto &[idx, impl] : m_internal) {
            unresolved.emplace(impl->raw(), impl.get());
        }

        auto r = registry{std::exchange(m_internal, component_map{})};
        for (const auto &[raw, impl] : unresolved) {
            impl->connect(r);
        }

        return r;
    }

    /**
     * @brief Resolves a registrar with one or more external components.
     */
    auto registrar::resolve_merged() -> registry
    {
        auto resolved = std::unordered_set<void *>{};
        resolved.reserve(m_external.size());

        for (const auto &[idx, impl] : m_external) {
            resolved.emplace(impl->raw());
        }

        auto unresolved = std::unordered_map<void *, component_base *>{};
        unresolved.reserve(m_internal.size());

        for (const auto &[idx, impl] : m_internal) {
            const auto raw = impl->raw();
            if (!resolved.contains(raw)) {
                unresolved.emplace(raw, impl.get());
            }
        }

        auto components = std::exchange(m_internal, component_map{});
        components.merge(std::exchange(m_external, component_map{}));

        auto r = registry{std::move(components)};
        for (const auto &[raw, impl] : unresolved) {
            impl->connect(r);
        }

        return r;
    }

}
