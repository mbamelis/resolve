#include "resolve/component.hpp"

namespace resolve
{
    namespace
    {
        struct my_custom_component
        {
            auto connect(const registry &) -> void
            {
            }
        };

        struct my_simple_component : simple_component
        {
        };
    }

    static_assert(component<my_custom_component>);
    static_assert(component<my_simple_component>);
}
