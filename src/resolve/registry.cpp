#include "resolve/registry.hpp"

#include <utility>

namespace resolve
{
    registry::registry(registrar::component_map &&c) noexcept
        : m_components{std::move(c)}
    {
    }
}
