#pragma once
#ifndef RESOLVE_EXCEPTION_HPP
#define RESOLVE_EXCEPTION_HPP

#include <exception>
#include <string>
#include <typeindex>

namespace resolve
{
    class exception : public std::exception
    {
    };

    /**
     * @brief Exception thrown by a `registrar` when the same interface is inserted more than once.
     */
    class duplicate_interface_error : public exception
    {
    public:
        duplicate_interface_error(std::type_index type);

        auto type_index() const noexcept -> std::type_index;
        auto what() const noexcept -> const char * override;

    private:
        std::type_index m_type;
        mutable std::string m_what;
    };

    /**
     * @brief Exception thrown by a `registry` when trying to resolve an interface that is not registered.
     */
    class unknown_interface_error : public exception
    {
    public:
        unknown_interface_error(std::type_index type);

        auto type_index() const noexcept -> std::type_index;
        auto what() const noexcept -> const char * override;

    private:
        std::type_index m_type;
        mutable std::string m_what;
    };
}

#endif
