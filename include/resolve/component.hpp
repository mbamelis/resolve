#pragma once
#ifndef RESOLVE_COMPONENT_HPP
#define RESOLVE_COMPONENT_HPP

#include <concepts>

namespace resolve
{
    class registry;

    template <typename TComponent>
    concept component = requires(TComponent &&c, const registry &r) { c.connect(r); };

    class simple_component
    {
    public:
        explicit simple_component() = default;
        virtual ~simple_component() = default;

        auto connect([[maybe_unused]] const registry &) -> void
        {
        }
    };

    static_assert(component<simple_component>);
}

#endif
