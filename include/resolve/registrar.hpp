#pragma once
#ifndef RESOLVE_REGISTRAR_HPP
#define RESOLVE_REGISTRAR_HPP

#include "resolve/component.hpp"

#include <memory>
#include <type_traits>
#include <typeindex>
#include <unordered_map>

namespace resolve
{
    class registrar
    {
        friend class registry;

    public:
        // TODO Allow multi-interface insert?

        registrar() noexcept = default;

        registrar(const registrar &) = delete;
        registrar(registrar &&) = default;

        auto operator=(const registrar &) -> registrar & = delete;
        auto operator=(registrar &&) -> registrar & = default;

        /**
         * @brief Resolves all registered components by calling their `connect` method.
         *
         * @post The `registrar` is in a default-constructed state.
         */
        auto resolve() -> registry;

        template <typename TInterface>
        auto contains() const -> bool;

        auto insert(registry &&r) -> void;

        template <typename TInterface, component TComponent>
            requires std::is_base_of_v<TInterface, std::remove_reference_t<TComponent>>
        auto insert(TComponent &&c) -> TComponent &;

        template <component TComponent>
        auto insert(TComponent &&c) -> TComponent &;

        template <typename TInterface, component TComponent, typename... TArgs>
            requires std::is_base_of_v<TInterface, TComponent>
        auto emplace(TArgs &&...args) -> TComponent &;

        template <component TComponent, typename... TArgs>
        auto emplace(TArgs &&...args) -> TComponent &;

        template <typename TInterface, component TComponent, typename... TArgs>
            requires std::is_base_of_v<TInterface, TComponent>
        [[nodiscard]] auto make_and_insert(TArgs &&...args) -> std::unique_ptr<TComponent>;

        template <component TComponent, typename... TArgs>
        [[nodiscard]] auto make_and_insert(TArgs &&...args) -> std::unique_ptr<TComponent>;

    private:
        struct component_base
        {
            virtual ~component_base() = default;
            virtual auto get() -> void * = 0;
            virtual auto raw() -> void * = 0;
            virtual auto connect(const registry &r) -> void = 0;
        };

        template <typename TInterface, typename TComponent>
        struct component_impl_val;

        template <typename TInterface, typename TComponent>
        struct component_impl_ref;

        auto resolve_simple() -> registry;
        auto resolve_merged() -> registry;

        template <typename TInterface>
        auto try_emplace() -> std::unique_ptr<component_base> &;

        using component_map = std::unordered_map<std::type_index, std::unique_ptr<component_base>>;
        component_map m_internal;
        component_map m_external;
    };
}

#include "resolve/registrar.ipp"
#endif
