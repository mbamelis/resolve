#pragma once
#ifndef RESOLVE_REGISTRY_HPP
#define RESOLVE_REGISTRY_HPP

#include "registrar.hpp"

#include <memory>
#include <typeindex>
#include <unordered_map>

namespace resolve
{
    class registry
    {
        friend class registrar;

    public:
        registry() noexcept = delete;

        registry(const registry &) = delete;
        registry(registry &&) = default;

        auto operator=(const registry &) -> registry & = delete;
        auto operator=(registry &&) -> registry & = default;

        ~registry() = default;

        template <typename TInterface>
        [[nodiscard]] auto get() const -> TInterface &;

        template <typename TInterface>
        [[nodiscard]] auto try_get() const -> TInterface *;

        template <typename TInterface>
        auto get(TInterface *&i) const -> void;

        template <typename TInterface>
        auto try_get(TInterface *&i) const -> bool;

    private:
        registry(registrar::component_map &&c) noexcept;

        template <typename TInterface>
        auto try_resolve_required() const -> TInterface &;

        template <typename TInterface>
        auto try_resolve_optional() const -> TInterface *;

        registrar::component_map m_components;
    };
}

#include "resolve/registry.ipp"
#endif
