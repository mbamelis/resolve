#pragma once
#ifndef RESOLVE_REGISTRAR_IPP
#define RESOLVE_REGISTRAR_IPP

#include "resolve/exception.hpp"

#include <typeinfo>
#include <utility>

namespace resolve
{
    template <typename TInterface, typename TComponent>
    struct registrar::component_impl_val : component_base
    {
        template <typename... TArgs>
        component_impl_val(TArgs &&...args)
            : m_component{std::forward<TArgs>(args)...}
        {
        }

        // clang-format off
        ~component_impl_val() override = default;
        auto get() -> void * override { return std::addressof(static_cast<TInterface &>(m_component)); }
        auto raw() -> void * override { return std::addressof(m_component); }
        auto connect(const registry &r) -> void override { m_component.connect(r); }
        auto ref() -> TComponent & { return m_component; }
        // clang-format on

    private:
        TComponent m_component;
    };

    template <typename TInterface, typename TComponent>
    struct registrar::component_impl_ref : component_base
    {
        component_impl_ref(TComponent &c)
            : m_component{c}
        {
        }

        // clang-format off
        ~component_impl_ref() override = default;
        auto get() -> void * override { return std::addressof(static_cast<TInterface &>(m_component.get())); }
        auto raw() -> void * override { return std::addressof(m_component.get()); }
        auto connect(const registry &r) -> void override { m_component.get().connect(r); }
        auto ref() -> TComponent & { return m_component.get(); }
        // clang-format on

    private:
        std::reference_wrapper<TComponent> m_component;
    };

    template <typename TInterface>
    auto registrar::contains() const -> bool
    {
        static_assert(std::is_same_v<TInterface, std::remove_cvref_t<TInterface>>);

        const auto &idx = std::type_index{typeid(TInterface)};
        return m_internal.contains(idx) || m_external.contains(idx);
    }

    template <typename TInterface, component TComponent>
        requires std::is_base_of_v<TInterface, std::remove_reference_t<TComponent>>
    auto registrar::insert(TComponent &&c) -> TComponent &
    {
        static_assert(std::is_same_v<TInterface, std::remove_cvref_t<TInterface>>);
        static_assert(!std::is_const_v<std::remove_reference_t<TComponent>>);

        auto &ptr = try_emplace<TInterface>();

        using TImpl = std::conditional_t<
            std::is_lvalue_reference_v<TComponent>,
            component_impl_ref<TInterface, std::remove_reference_t<TComponent>>,
            component_impl_val<TInterface, std::remove_reference_t<TComponent>>>;

        auto impl = std::make_unique<TImpl>(std::forward<TComponent>(c));
        auto &ref = impl->ref();

        // TODO No need to call impl->ref() if `c` is a lvalue ref, can just return that

        ptr = std::move(impl);
        return ref;
    }

    template <component TComponent>
    auto registrar::insert(TComponent &&c) -> TComponent &
    {
        using TInterface = std::remove_reference_t<TComponent>;
        return insert<TInterface, TComponent &&>(std::forward<TComponent>(c));
    }

    template <typename TInterface, component TComponent, typename... TArgs>
        requires std::is_base_of_v<TInterface, TComponent>
    auto registrar::emplace(TArgs &&...args) -> TComponent &
    {
        static_assert(std::is_same_v<TInterface, std::remove_cvref_t<TInterface>>);
        static_assert(std::is_same_v<TComponent, std::remove_cvref_t<TComponent>>);
        static_assert(!std::is_const_v<TComponent>);

        auto &ptr = try_emplace<TInterface>();

        auto impl = std::make_unique<component_impl_val<TInterface, TComponent>>(std::forward<TArgs>(args)...);
        auto &ref = impl->ref();

        ptr = std::move(impl);
        return ref;
    }

    template <component TComponent, typename... TArgs>
    auto registrar::emplace(TArgs &&...args) -> TComponent &
    {
        using TInterface = TComponent;
        return emplace<TInterface, TComponent>(std::forward<TArgs>(args)...);
    }

    template <typename TInterface, component TComponent, typename... TArgs>
        requires std::is_base_of_v<TInterface, TComponent>
    auto registrar::make_and_insert(TArgs &&...args) -> std::unique_ptr<TComponent>
    {
        static_assert(std::is_same_v<TInterface, std::remove_cvref_t<TInterface>>);
        static_assert(std::is_same_v<TComponent, std::remove_cvref_t<TComponent>>);
        static_assert(!std::is_const_v<TComponent>);

        auto &ptr = try_emplace<TInterface>();

        auto comp = std::make_unique<TComponent>(std::forward<TArgs>(args)...);
        auto impl = std::make_unique<component_impl_ref<TInterface, TComponent>>(*comp);
        ptr = std::move(impl);

        return comp;
    }

    template <component TComponent, typename... TArgs>
    auto registrar::make_and_insert(TArgs &&...args) -> std::unique_ptr<TComponent>
    {
        using TInterface = TComponent;
        return make_and_insert<TInterface, TComponent>(std::forward<TArgs>(args)...);
    }

    template <typename TInterface>
    auto registrar::try_emplace() -> std::unique_ptr<component_base> &
    {
        const auto &idx = std::type_index{typeid(TInterface)};
        auto [it, inserted] = m_internal.emplace(idx, nullptr);

        if (!inserted) {
            throw duplicate_interface_error{idx};
        }

        return it->second;
    }
}

#endif
