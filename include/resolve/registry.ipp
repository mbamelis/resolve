#pragma once
#ifndef RESOLVE_REGISTRY_IPP
#define RESOLVE_REGISTRY_IPP

#include "resolve/exception.hpp"

#include <sstream>
#include <typeinfo>

namespace resolve
{
    template <typename TInterface>
    auto registry::get() const -> TInterface &
    {
        return try_resolve_required<TInterface>();
    }

    template <typename TInterface>
    auto registry::try_get() const -> TInterface *
    {
        return try_resolve_optional<TInterface>();
    }

    template <typename TInterface>
    auto registry::get(TInterface *&i) const -> void
    {
        i = std::addressof(try_resolve_required<TInterface>());
    }

    template <typename TInterface>
    auto registry::try_get(TInterface *&i) const -> bool
    {
        i = try_resolve_optional<TInterface>();
        return static_cast<bool>(i);
    }

    template <typename TInterface>
    auto registry::try_resolve_required() const -> TInterface &
    {
        const auto &idx = std::type_index{typeid(TInterface)};
        if (auto it = m_components.find(idx); it != m_components.end()) {
            return *static_cast<TInterface *>(it->second->get());
        }
        throw unknown_interface_error{idx};
    }

    template <typename TInterface>
    auto registry::try_resolve_optional() const -> TInterface *
    {
        const auto &idx = std::type_index{typeid(TInterface)};
        if (auto it = m_components.find(idx); it != m_components.end()) {
            return static_cast<TInterface *>(it->second->get());
        }
        return nullptr;
    }
}

#endif
